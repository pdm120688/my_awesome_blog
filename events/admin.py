from django.contrib import admin

from blog.apps import BlogConfig
from my_awesome_blog.settings import BASE_DIR
from .models import Event

# Register your models here.
admin.site.register(Event)
